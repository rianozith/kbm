    
    <div class="form-group">
    	{!! Form::label('nama_panggilan', 'Nama panggilan') !!}
    	{!! Form::text('nama_panggilan', null, [
    		'class'=>'form-control input-md', 
    		'placeholder'=>'Nama Panggilan']) !!}
    </div>
    <div class="form-group">
    	{!! Form::label('gender', 'gender') !!}
    	{!! Form::text('gender', null, [
    		'class'=>'form-control input-md', 
    		'placeholder'=>'gender']) !!}
    </div>
    <div class="form-group">
    	{!! Form::label('tempat_lahir', 'tempat lahir') !!}
    	{!! Form::text('tempat_lahir', null, [
    		'class'=>'form-control input-md', 
    		'placeholder'=>'tempat lahir']) !!}
    </div>
    <div class="form-group">
    	{!! Form::label('tgl_lahir', 'tanggal lahir') !!}
    	{!! Form::text('tgl_lahir', null, [
    		'class'=>'form-control input-md', 
    		'placeholder'=>'tgl lahir ']) !!}
    </div>
    <div class="form-group">
    	{!! Form::label('agama', 'agama') !!}
    	{!! Form::text('agama', null, [
    		'class'=>'form-control input-md', 
    		'placeholder'=>'agama']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('gol_darah', 'gol darah') !!}
        {!! Form::text('gol_darah', null, [
            'class'=>'form-control input-md', 
            'placeholder'=>'gol darah']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('anak_ke', 'anak ke') !!}
        {!! Form::text('anak_ke', null, [
            'class'=>'form-control input-md', 
            'placeholder'=>'anak ke']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('jml_saudara', 'jml saudara') !!}
        {!! Form::text('jml_saudara', null, [
            'class'=>'form-control input-md', 
            'placeholder'=>'jml saudara']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('foto', 'foto') !!}
        {!! Form::text('foto', null, [
            'class'=>'form-control input-md', 
            'placeholder'=>'foto']) !!}
    </div>